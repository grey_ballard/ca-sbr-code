#include "aux.h"
#include "casbr.h"
#include <pthread.h>
#include <string.h>

void *thread_routine_sbr (void*);
double check_answer (int,int,double*,int,double*,int);

pthread_barrier_t barrier;

int main( int argc, char **argv )
{    
  // Loop variables
  int i, j, pp, ppp, nnn, bbb, iter, max_its;

  // Timing variables
  double seconds, eff_mflops, true_mflops;

  // Algorithmic parameters
  int n,b,c,d,omega,ell,slab,cfcn,p;

  // Band storage variables
  double *band = NULL;
  double *band2 = NULL;
  size_t n_words;
  int lda;
      
  // for checking correctness
  int check = 0;
  double *output = NULL;
  int ldoutput;
  double *backup = NULL; 
  int ldbackup;
  double max_absval_error;
  
  // Assorted:
  int length;
  int bb,dd;

  // Tuning space variables
  int bs[] = {50, 100, 150, 200, 250, 300}; 
  int b_max = 300;
  int ps[] =  {1,10};
  int p_max = 10;
  int ns[] = {4000,8000,12000,16000,20000,24000};
  
  // Tuning space constants:
  omega = 1;
  ell = 1;     
  slab = 32;
  max_its = 2;
  
  // If current bandwidth is greater than thresholds[i] + threshold_tol,
  // reduce to thresholds[i]. Otherwise reduce to thresholds[i-1], where
  // thresholds[-1] is 1 (implicitly). For example, if b_1 = 300, then
  // b_2 = 200, b_3 = 50, and b_4 = 1 (3 sweeps).
  int thresholds[2] = {50, 200};
  int threshold_tol = 50;

  // Allocate pthread resources
  pthread_attr_t attr;
  pthread_attr_init(&attr);
  pthread_t *threads = (pthread_t*) malloc(p_max * sizeof(pthread_t));
  arguments *args    = (arguments*) malloc(p_max * sizeof(arguments));

fprintf(stderr,"n, p, b, omega, ell, slab, seconds, eff_mflops, true_mflops, max_absval_error\n");


  for (nnn = 0; nnn < sizeof(ns)/sizeof(ns[0]); ++nnn)
  {
    n = ns[nnn];
    
    // Allocate a band matrix sufficiently large for all runs
  n_words = (size_t) (n+b_max)*(2*b_max);
  band = (double*) malloc(n_words*sizeof(double));
  band2 = (double*) malloc(n_words*sizeof(double));
  
  if (check != 0)
    backup = (double*) malloc (n_words*sizeof(double));
  
  for (ppp = 0; ppp < sizeof(ps)/sizeof(ps[0]); ++ppp)
  {
    p = ps[ppp];
    
    // (re-) Allocate synchronization arrays
    first_col = (volatile int*) malloc( p * sizeof( int ) );
    last_col = (volatile int*) malloc( p * sizeof( int ) );
    set = (volatile int*) malloc( p * sizeof( int ) );

    // Initialize pthread barrier:
    pthread_barrier_init( &barrier, NULL, p );

    for (bbb = 0; bbb < sizeof(bs)/sizeof(bs[0]); ++bbb)
    {
       b = bs[bbb];

       if (b > thresholds[1] + threshold_tol)
          d = b - thresholds[1];
        else if (b > thresholds[0] + threshold_tol)
          d = b - thresholds[0];
        else
          d = b - 1;
    
	// Store d extra diagonals
	lda = b+d+1;

   
	  
	  seconds = 0.;
	  max_absval_error = -1.;
     for (iter = 1; iter <= max_its; ++iter)
     {

    /* Fill in band with random numbers and store explicit zeros
    in the last d diagonals and the trailing trapezoid: /==|  */
    length = b + 1;
	for (i = 0; i < n + b; ++i)
	{
		if (i >= n - b ) 
			length = n - i;
		for (j = 0; j < length; ++j)
			band[j + i*lda] = drand48();
	    for( ; j < lda; ++j)
	        band[j + i*lda] = 0.;
	}
	
    // For checking correctness
    if (check != 0)
    {
      ldbackup = b+1;
      for (i = 0; i < n; ++i)
        memcpy(backup + i*ldbackup,band + i*lda,ldbackup*sizeof(double));
   }
    
    
	  // Set thread arguments
	  for (pp = 0; pp < p; ++pp)
	  {
	    args[pp].n    = n;
	    args[pp].b    = b;
	    args[pp].mult = omega;
	    args[pp].hops = ell;
	    args[pp].slab = slab;
	    args[pp].n_threads = p;
	    args[pp].thread_id = pp;
	    args[pp].thresholds[0] = thresholds[0];
	    args[pp].thresholds[1] = thresholds[1];
	    args[pp].threshold_tol = threshold_tol;

	    args[pp].A = band;
	    args[pp].band2 = band2;
	    args[pp].lda = lda;
	  }




	  for (pp = 1; pp < p; ++pp) 
	    pthread_create( &threads[pp], &attr, thread_routine_sbr, &args[pp] );

	  thread_routine_sbr ( &args[0] );

	  for (pp = 1; pp < p; ++pp) 
	    pthread_join( threads[pp], NULL );

     seconds += args[0].seconds;
     
     if (check != 0)
     {
     output = args[0].A;
     ldoutput = args[0].lda;
     double max_absval_error_tmp = check_answer(n,b,backup,ldbackup,output,ldoutput);
     max_absval_error = MAX (max_absval_error, max_absval_error_tmp);
    }
     else
      max_absval_error = -1;
      
      
    }
    seconds /= max_its;
    
     eff_mflops = 4.e-6*n*n*b/seconds;
     
     true_mflops = 0; bb = b;
     if (bb > thresholds[1] + threshold_tol)
     {
          dd = bb - thresholds[1];
          true_mflops += 4.e-6*n*n*dd + 2.e-6*n*n*dd*dd/bb;
          bb = thresholds[1];
          }
     if (bb > thresholds[0] + threshold_tol)
     {
          dd = bb - thresholds[0];
          true_mflops += 4.e-6*n*n*dd + 2.e-6*n*n*dd*dd/bb;
          bb = thresholds[0];
          }
    if (bb > 1)
    {
          dd = bb - 1;
          true_mflops += 4.e-6*n*n*dd + 2.e-6*n*n*dd*dd/bb;
          }
      true_mflops /= seconds;

	  // n, p, b, mult, ell, slab, seconds, eff_mflops, true_mflops, max_absval_error
	  char *ostring = "%d,%d,%d,%d,%d,%d,%e,%e,%e,%e\n";
	  fprintf(stderr,ostring,n,p,b,omega,ell,slab,seconds,eff_mflops,true_mflops,max_absval_error);

   } // endfor b

    pthread_barrier_destroy( &barrier );
    free ( (int*) first_col);
    free ( (int*) last_col);
    free ( (int*) set);
    
    } // endfor p
    free (band);
    free (band2);
    
    if (check != 0)
      free (backup);
       
  } // endfor n

  // Free pthread resources
  pthread_attr_destroy( &attr );
  free( args );
  free( threads );

  return 0;
}

void *thread_routine_sbr( void *params )
{
  arguments args = *(arguments*) params;	

  args.work = (double*) malloc(3*args.b*args.b*sizeof(double));

  // For repacking the band in parallel:
  int i, length, lda_old = args.lda;
  int repack = 0;
  int blk = (int) (args.n + args.b + args.n_threads - 1)/args.n_threads;
  int mystart = args.thread_id * blk;
  int myend = MIN((args.thread_id+1)*blk,args.n);
  double *src = args.band2,*dst = args.A,*tmp=NULL;
  
  // Start timer:
  pthread_barrier_wait( &barrier );
  if (args.thread_id == 0)
    args.seconds = -wall_time();
 
 // "First" sweep - reduce to threshold[1]
  if (args.b > args.thresholds[1] + args.threshold_tol)
  {
    first_col[args.thread_id] = -1;
    last_col[args.thread_id] = -1;
    set[args.thread_id] = -1;
    
    args.d = args.b - args.thresholds[1];
    args.c = args.thresholds[1];
    args.chasefcn = ( args.c == 1 ? 0 : 1 );
    lda_old = args.lda;
    args.lda = args.b + args.d + 1;
    
    if (repack == 1)
    {
      	tmp = dst;
  		dst = src;
  		src = tmp;
        
        // Repack band
    length = args.b+1;
	for (i = mystart; i < myend; ++i)
	{
		if (i >= args.n - args.b ) 
			length = args.n - i;
	     if (length > 0)
	     {
	     memcpy(dst + i*args.lda, src+i*lda_old, length*sizeof(double));
	      memset(dst + i*args.lda + length, 0., (args.lda-length)*sizeof(double));
	     }
	     else
	      memset(dst + i*args.lda, 0., args.lda*sizeof(double));
	}

  		args.A = dst;
    }
    
    pthread_barrier_wait( &barrier );
    
    par_sweep( &args );
    
    pthread_barrier_wait( &barrier );
    
    args.b = args.thresholds[1];
    repack = 1;
  }
  
  // "Second" sweep - reduce to threshold[0]
  if (args.b > args.thresholds[0] + args.threshold_tol)
  {
	first_col[args.thread_id] = -1;
    last_col[args.thread_id] = -1;
    set[args.thread_id] = -1;
    
    args.d = args.b - args.thresholds[0];
    args.c = args.thresholds[0];
    args.chasefcn = ( args.c == 1 ? 0 : 1 );
    lda_old = args.lda;
    args.lda = args.b + args.d + 1;
    
    if (repack == 1)
    {
      	tmp = dst;
  		dst = src;
  		src = tmp;
        
              // Repack band
    length = args.b+1;
	for (i = mystart; i < myend; ++i)
	{
		if (i >= args.n - args.b ) 
			length = args.n - i;
	     if (length > 0)
	     {
	     memcpy(dst + i*args.lda, src+i*lda_old, length*sizeof(double));
	      memset(dst + i*args.lda + length, 0., (args.lda-length)*sizeof(double));
	     }
	     else
	      memset(dst + i*args.lda, 0., args.lda*sizeof(double));
	}

  		args.A = dst;
    }
    
    pthread_barrier_wait( &barrier );
    
    par_sweep( &args );
    
    pthread_barrier_wait( &barrier );
    
    args.b = args.thresholds[0];
    repack = 1;
  }
  
  if (args.b > 1)
    {
	first_col[args.thread_id] = -1;
    last_col[args.thread_id] = -1;
    set[args.thread_id] = -1;
    
    args.d = args.b - 1;
    args.c = 1;
    args.chasefcn = ( args.c == 1 ? 0 : 1 );
    lda_old = args.lda;
    args.lda = args.b + args.d + 1;
    
    if (repack == 1)
    {
        tmp = dst;
  		dst = src;
  		src = tmp;
        
          // Repack band
    length = args.b+1;
	for (i = mystart; i < myend; ++i)
	{
		if (i >= args.n - args.b ) 
			length = args.n - i;
	     if (length > 0)
	     {
	     memcpy(dst + i*args.lda, src+i*lda_old, length*sizeof(double));
	      memset(dst + i*args.lda + length, 0., (args.lda-length)*sizeof(double));
	     }
	     else
	      memset(dst + i*args.lda, 0., args.lda*sizeof(double));
	}

  		args.A = dst;
  	}
    
    pthread_barrier_wait( &barrier );
    
    par_sweep( &args );
    
    pthread_barrier_wait( &barrier );
  }

  if (args.thread_id == 0)
  {
    args.seconds += wall_time();
    ((arguments*)params)->seconds = args.seconds;
    ((arguments*) params)->A = args.A;
    ((arguments*) params)->lda = args.lda;
  }
  pthread_barrier_wait( &barrier );

  free (args.work);
  

  
  return NULL;
}

// Check against LAPACK driver. This can take a while!
double check_answer (int n, int b, double *A, int lda, double *T, int ldt)
{
   char *VECT = "N";
   char *UPLO = "L";
   int N = n;
   int KD = b;
   double *AB = (double*) calloc( n*(b+1), sizeof(double) );
   int LDAB = b+1;
   double *D = (double*) malloc( n * sizeof(double) );
   double *E = (double*) calloc( n, sizeof(double) ); // Only needs to be n-1
   double *Q = NULL;
   int LDQ = 0;
   double *WORK = (double*) malloc( n * sizeof(double) );
   int INFO;
  
  int i, length = b+1;
  for (i=0; i<n; i++)
  {
    if (i >= n-b ) length = n - i;
    memcpy(AB+i*LDAB,A+i*lda,length*sizeof(double));
  }

  DSBTRD(VECT,UPLO,&N,&KD,AB,&LDAB,D,E,Q,&LDQ,WORK,&INFO);

  double *tmp = malloc(2*n*sizeof(double));
  for (i = 0; i < n; ++i)
  {
     tmp[2*i] = D[i];
     tmp[2*i+1] = E[i];
  }
  
  double ret = max_absval_diff( n, 1, T, ldt, tmp, 2 );
  free(tmp);
  free (AB);
  free (D);
  free (E);
  free (WORK);
  
  return ret;
}
