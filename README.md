CA-SBR-CODE

Grey Ballard and Nick Knight

ballard@wfu.edu

nknight@nyu.edu

This code was used to produce the results in the following paper:

G. Ballard, J. Demmel and N. Knight, Communication avoiding successive band reduction, 
Proceedings of the 17th ACM SIGPLAN symposium on Principles and Practice of Parallel 
Programming, PPoPP '12, ACM, New York, NY, USA, pp. 35-44, February 2012, 
http://doi.acm.org/10.1145/2145816.2145822.

Quickstart:

0. Uses bash + icc + MKL. 
1. Run iccvars.sh (or your equivalent, otherwise see Makefile). 
2. make
3. numactl --membind=0 --cpubind=0 ./casbr

I) Specifying problem sizes

The executable "casbr" will run a bunch of problems. These are hardcoded into
benchmark.c. Our defaults are:

bandwidths = {50, 100, 150, 200, 250, 300}
number of threads = {1,10}
dimensions = {4000,8000,12000,16000,20000,24000}
max_its = 2 (we average runtime over this many iterations)

II) Specifying parameters

See the paper for details. In benchmark.c, we expose the following:

omega - number of bulges chased at a time (default = 1)
ell - number of hops for each bulge (default = 1)
slab - inner blocking factor for BLAS3 Householder transformations
thresholds[2], threshold_tol - reduction sequence. See comments in benchmark.c

III) Output and error checking

Output to stderr. Note that eff_mflops assumes 4n^2b flops were performed, like
LAPACK dsbtrd. SBR does as many as 6n^2b flops, depending on the reduction sequence;
we report true_mflops as the actual flops divided by runtime. See paper for details
on counting arithmetic.

By setting check = 1 in benchmark.c, the code calls (MKL) dsbtrd and compares
the resulting tridiagonal matrix with the one from CA-SBR. We report the maximum
absolute value difference componentwise, an absolute measure. 
The tridiagonal matrix should be unique up to sign.

If check = 0, we report -1 for the error. This is the default.

IV) Running with numactl

We perform no NUMA optimizations, so we always ran using numactl to bind to a socket.
Performance deteriorated when threads were allocated across sockets. Note that the default
number of threads (10) matches our machine but may not match yours!

V) Linking with MKL+icc

We used icc 11.1 and MKL 10.3. In order to use a different BLAS/LAPACK, you may need to change
the Makefile as well as the BLAS/LAPACK wrappers in casbr.h. 

For example, to use the usual FORTRAN bindings, change the -DMKL to -DDEFAULT in the Makefile
so the correct preprocessor directives will be called (adds trailing underscores). Again,
just look at the Makefile and casbr.h to make sure the wrappers are correct for your BLAS/LAPACK.

