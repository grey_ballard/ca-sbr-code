# On boxboro, run ". ./iccvars.sh" in the boxboro/ directory
# or the equivalent script that came with your icc+MKL install
# otherwise set MKLROOT to your MKL path.
# MKLROOT :=  /opt/intel/Compiler/11.1/073/mkl

CC = icc

DEBUG := -g -O0 -Wall
OPT := -O3 -fast
INCLUDES := -I$(MKLROOT)/include
CFLAGS := -m64 $(OPT) -c $(INCLUDES) -DMKL

MKLSEQ := -L$(MKLROOT)/lib/em64t -lmkl_intel_lp64 -lmkl_sequential -lmkl_core -lpthread
MKLPAR := -L$(MKLROOT)/lib/em64t -lmkl_intel_lp64 -lmkl_intel_thread -lmkl_core -openmp -lpthread
LDFLAGS := 
LDLIBS_SEQ := $(MKLSEQ) -lrt
LDLIBS_PAR := $(MKLPAR) -lrt

objects = benchmark.o aux.o kernels.o
targets = casbr dynprog mkl

.PHONY : default
default : all

.PHONY : all
all : clean $(targets)

casbr : benchmark.o aux.o kernels.o
	$(CC) $(LDFLAGS) $^ $(LDLIBS_SEQ) -o $@
	
dynprog : benchmark_dynprog.o aux.o kernels.o
	$(CC) $(LDFLAGS) $^ $(LDLIBS_SEQ) -o $@
	
mkl : benchmark_mkl.o aux.o
	$(CC) $(LDFLAGS) $^ $(LDLIBS_PAR) -o $@

benchmark.o : benchmark.c aux.h casbr.h
	$(CC) $(CFLAGS) $< -o $@
	
aux.o : aux.c aux.h
	$(CC) $(CFLAGS) $< -o $@

kernels.o : kernels.c aux.h casbr.h
	$(CC) $(CFLAGS) $< -o $@

benchmark_dynprog.o : benchmark_dynprog.c aux.h casbr.h
	$(CC) $(CFLAGS) $< -o $@
	
benchmark_mkl.o : benchmark_mkl.c aux.h
	$(CC) $(CFLAGS) $< -o $@

.PHONY : clean
clean :
	rm -rf $(objects) $(targets) *~
