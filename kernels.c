#include "casbr.h"
#include "aux.h"

double max_eigval_diff( int n, double *A, int ba, int lda, double *B, int bb, int ldb, double *work )
{
	char *vect = "N", *uplo = "L";
	double *Q = NULL, *Ea, *Eb, max_diff = 0;
	int i, info;
	
	/* work must be of size 5n-2 */
	Ea = work + 3*n-2;
	Eb = Ea + n;
	
	/* compute eigenvalues of A */
	DSBEV( vect, uplo, &n, &ba, A, &lda, Ea, Q, &n, work, &info);
	if( info != 0 )
		die("dsbev failed on first banded matrix in max_eigval_diff");
	
	/* compute eigenvalues of B */
	DSBEV( vect, uplo, &n, &bb, B, &ldb, Eb, Q, &n, work, &info);
	if( info != 0 )
		die("dsbev failed on second banded matrix in max_eigval_diff");
	
	for (i = 0; i < n; i++ )
	{
		max_diff = MAX( max_diff, fabs(Ea[i]-Eb[i])/fabs(Eb[i]) );
	  
	}
		    
	return max_diff;
	
}

double max_absval_diff( int n, int b, double *A, int lda, double *B, int ldb )
{
	int i, j;
	double max_diff = 0;
	
	/* compare the difference in absolute values of band matrix entries */
	/* this shows correctness assuming reduction is unique up to sign;
	 * this is an absolute measure of error */
	for ( i = 0; i < n; i++ )
		for ( j = 0; j <= b; j++ )
			max_diff = MAX( max_diff, fabs( fabs(A[i*lda+j])-fabs(B[i*ldb+j]) ) );
	
	return max_diff;
}

void set_gap( arguments* args )
{
	if ( 2*(args->c)+(args->d) <= (args->b) )
		args->gap = 1;
	else
		args->gap = 2; 
}

/* chase one bulge, if bulge off end of band, return false */
int chase2( double* A, int lda, int n, int b, int c, int d, int par, int blg, double* work )
{
    int i, j, Hh_len = d+1, qr_cols, pre_cols, post_rows, ldar = lda-1, ione = 1;
    char *L = "L", *R = "R";
    double alpha, dzero = 0.0, done = 1.0, dmone = -1.0, dmhalf = -0.5;
    
    /* if parallelogram or bulge is off the band, return zero */
    if (par*c + blg*b >= n-b+d-1)
        return 0;
    
    double *Ast, *Aqr, *Apre, *Asym, *Asym_pre, *Asym_post, *Apost, *tau, *diag;
    
    /* Divy up workspace to tau (for Hh vectors), diag (for storing accumulators), and workspace for LAPACK functions */
    tau = work;
    diag = work+c;
    work = work+2*c;
    
    /* Set Ast to first entry of parallelogram or bulge */
    Ast = A + (b-d) + (par*c)*lda;
    if (blg)
        Ast += d + (blg*b-d)*lda;
    
    /* QR */
    Aqr = Ast;
    qr_cols = c-1;
    for (i = 0; i < c; i++)
    {
        /* compute Householder vector */
        DLARFG( &Hh_len, Aqr, Aqr+1, &ione, tau+i);
		
        /* store accumulated value and overwrite with explicit one */
        diag[i] = *Aqr;
        *Aqr = done;  
        
        /* apply Hh vector to remaining columns in QR */
        DLARF( L, &Hh_len, &qr_cols, Aqr, &ione, tau+i, Aqr+ldar, &ldar, work );
        
        /* advance Aqr down diagonal */
        Aqr += lda;
        
        qr_cols--;
    }
    
    /* PRE */
    Aqr = Ast;
    Apre = Ast+c*ldar;
    if (blg)
        pre_cols = b-c;
    else
        pre_cols = b-d-c;
    for (i = 0; i < c; i++)
    {
        DLARF( L, &Hh_len, &pre_cols, Aqr, &ione, tau+i, Apre, &ldar, work );
        
        /* advance Apre down column and Aqr down diagonal */
        Apre++;
        Aqr += lda;
    }
    
    /* SYM */
    Aqr = Ast;
    if (blg)
        Asym = Ast+b*ldar;
    else
        Asym = Ast+(b-d)*ldar;
    Asym_pre = Asym;
    Asym_post = Asym+d+1;
    pre_cols = 0; post_rows = c-1;
    for (i = 0; i < c; i++)
    {     
        /* PRE inside SYM */
        DLARF( L, &Hh_len, &pre_cols, Aqr, &ione, tau+i, Asym_pre, &ldar, work );
        
        /* SYM inside SYM */
        DSYMV( L, &Hh_len, tau+i, Asym, &ldar, Aqr, &ione, &dzero, work, &ione );
        alpha = dmhalf*tau[i]*DDOT( &Hh_len, work, &ione, Aqr, &ione );
        DAXPY( &Hh_len, &alpha, Aqr, &ione, work, &ione );
        DSYR2( L, &Hh_len, &dmone, Aqr, &ione, work, &ione, Asym, &ldar );
        
        /* POST inside SYM */
        DLARF( R, &post_rows, &Hh_len, Aqr, &ione, tau+i, Asym_post, &ldar, work );
        
        /* advance Asym, Asym_post, and Aqr down diagonals and Asym_pre down column */
        Asym += lda;
        Asym_pre += 1;
        Asym_post += lda;
        Aqr += lda;
        
        pre_cols++;
        post_rows--;
    }
    
    /* POST */
    Aqr = Ast;
    if (blg)
        Apost = Ast+(c+d)+b*ldar;
    else
        Apost = Ast+(c+d)+(b-d)*ldar;
    post_rows = b-c+1;
    for (i = 0; i < c; i++)
    {
        DLARF( R, &post_rows, &Hh_len, Aqr, &ione, tau+i, Apost, &ldar, work );
        
        /* advance Apost along row and Aqr down diagonal */
        Apost += ldar;
        Aqr += lda;
        
        post_rows++;
    }
    
    /* clean up (restore accumulators and delete Hh entries) */
    Aqr = Ast;
    for (i = 0; i < c; i++)
    {
        Aqr[0] = diag[i];
        for( j = 1; j <= d; j++)
            Aqr[j] = 0;
        Aqr += lda;
    }
    
    return 1;
    
}

/* block square bulge into tall skinny slabs and use BLAS3 functions */
int chase3_blocked( double* A, int lda, int n, int b, int c, int d, int par, int blg, double* work, int bs )
{
    int i, j, k, qr_cols, Hh_len = d+1, pre_cols, post_rows, cc, ccd, num_slabs, ldar = lda-1, ione = 1;
    char *LL = "L", *RR = "R", *FF = "F", *CC = "C", *UU = "U", *NN = "N", *TT = "T";
    double dzero = 0.0, done = 1.0, dmone = -1.0, dhalf = 0.5;
    double *Aqr, *Apre, *Asym, *Apost3, *Apost2, *U, *T, *V, *tau;
    
    /* if parallelogram or bulge is off the band, return zero */
    if (par*c + blg*b >= n-b+d-1)
        return 0;
    
    /* Set cc to be number of cols of first slab */
    cc = MIN(c,bs);
    ccd = cc+d;
    
    /* Set pointers to workspace (use V for workspace in dlarf/dlarfb) */
    /* total space required is (bs+d)*bs + bs*bs + bs + (bs+d)*b */
    U    = work;
    T    = U + ccd*cc;
    tau  = T + cc*cc;
    V    = tau + cc;
	
    /* Set initial pointers to QR/PRE/SYM/POST */ 
    Aqr = A + (b-d) + (par*c)*lda;
    if (blg)
        Aqr += d + (blg*b-d)*lda;
    Apre = Aqr+cc*ldar;
    if (blg)
        Asym = Aqr+b*ldar;
    else
        Asym = Aqr+(b-d)*ldar;
    Apost3 = Asym + ccd;
    Apost2 = Apost3 + b-cc + lda;
    
    /* loop over slabs */
    num_slabs = CEIL(c,bs);
    for (k = 0; k < num_slabs; k++)
    {
        
        /* Set sizes of QR/PRE/POST (SYM is ccd x ccd) */
        qr_cols = cc-1;
        if (blg)
            pre_cols = b-cc; 
        else
            pre_cols = b-d-cc;
        post_rows = b-cc+1;
        
        /* Clear U */
        for (i=0; i<ccd*cc; i++)
            U[i] = dzero;
		
        /* QR */    
        for (i = 0; i < cc; i++)
        {
            /* compute Householder vector */
            DLARFG( &Hh_len, Aqr+i*lda, Aqr+i*lda+1, &ione, tau+i);
			
            /* copy Householder info to U, make sure other entries of U are zero (don't clear from band yet) */
            U[i+ccd*i] = done;
            DCOPY( &d, Aqr+1+i*lda, &ione, U+1+i+i*ccd, &ione );
			
            /* apply Hh vector to remaining columns in QR */
            DLARF( LL, &Hh_len, &qr_cols, U+i+i*ccd, &ione, tau+i, Aqr+(i+1)*lda-1, &ldar, V );
			
            qr_cols--;
        }   
		
        /* form T */
        DLARFT( FF, CC, &ccd, &cc, U, &ccd, tau, T, &cc );
		
        /* PRE */
        DLARFB( LL, TT, FF, CC, &ccd, &pre_cols, &cc, U, &ccd, T, &cc, Apre, &ldar, V, &pre_cols );
		
        /* POST */
        /* BLAS 3 part */
        DLARFB( RR, NN, FF, CC, &post_rows, &ccd, &cc, U, &ccd, T, &cc, Apost3, &ldar, V, &post_rows );
        /* BLAS 2 part */
        post_rows = 1;
        for (i = 1; i < cc; i++)
        {
            DLARF( RR, &post_rows, &Hh_len, U+i+i*ccd, &ione, tau+i, Apost2+(i-1)*ldar, &ldar, V );
            post_rows++;
        }
		
        /* SYM */ 
        /* U = U*T */
        DTRMM( RR, UU, NN, NN, &ccd, &cc, &done, T, &cc, U, &ccd );
        /* V = Asym * U */
        DSYMM( LL, LL, &ccd, &cc, &done, Asym, &ldar, U, &ccd, &dzero, V, &ccd );
        /* T = (1/2) * V^T * U */
        DGEMM( TT, NN, &cc, &cc, &ccd, &dhalf, V, &ccd, U, &ccd, &dzero, T, &cc );
		
        /* reset U with Hh vectors and clear from band */
        for (i=0; i<cc; i++)
        {
            for (j=0; j<i; j++)
                U[j+i*ccd] = dzero;
            U[i+i*ccd] = done; 
            DCOPY( &d, Aqr+1+i*lda, &ione, U+1+i+i*ccd, &ione );
            for (j=1; j<Hh_len; j++)
                Aqr[j+i*lda] = dzero; 
        }
		
        /* V = V - U * T */
        DGEMM( NN, NN, &ccd, &cc, &cc, &dmone, U, &ccd, T, &cc, &done, V, &ccd );
        /* Asym = Asym - V*U^T - U*V^T */
        DSYR2K( LL, NN, &ccd, &cc, &dmone, U, &ccd, V, &ccd, &done, Asym, &ldar );
        
        /* set cc to be number of columns annihilated in next pass */
        cc = MIN( c-(k+1)*bs, bs);
        ccd = cc+d;
        
        /* increment pointers for next slab */
        Aqr    += bs*lda;
        Apre   += bs*lda - (bs-cc)*ldar;
        Asym   += bs*lda;
        Apost3 += bs*lda - (bs-cc);
        Apost2 += bs*lda;
    }
	
    /* return successful chase */
    return 1;
	
}

/* wrapper function to choose which version of chase to call */
int chase( arguments* args, int par, int blg )
{
	if( args->c + args->d > args->b)
		die("tried to chase a bulge with c+d>b\n");	
	if (args->chasefcn == 0)
		return chase2(args->A, args->lda, args->n, args->b, args->c, args->d, par, blg, args->work);
	else
		return chase3_blocked(args->A, args->lda, args->n, args->b, args->c, args->d, par, blg, args->work, args->slab);
}

/* creates num_bulges bulges starting with bulge (lead_par,0)  */
void create_bulges(arguments* args, int* lead_par, int* lead_blg, int* num_bulges )
{

    int i, k, done, num_done = 0; 
    *lead_blg = 1-(args->gap);
    	
    /* create num_bulges bulges */
    for (i = 0; i < *num_bulges; i++)
    {
        /* make room for ith bulge */
        /* (start k at num_done to avoid chasing bulges already off end of band) */
        for (k = num_done; k < i; k++)
        {
            done = 0;
            
            /* chase previously created bulge once */
            if ( !chase( args, *lead_par+k, *lead_blg-(args->gap)*k ) )
            {
                num_done++;
                done = 1;
            }
            /* if gap is 2 then must chase it again */
            /* (also check to see if the first chase pushed it off end of band) */
            if ( args->gap == 2 && !done )
            {
                if ( !chase( args, *lead_par+k, *lead_blg-(args->gap)*k+1 ) )
                    num_done++;
            }
        }
        /* create ith bulge */
        if ( !chase( args, *lead_par+i, 0 ) )
            num_done++;
        /* increment lead_blg number */
        *lead_blg += args->gap;
    }
	
    /* update parameters if some bulges were chased off end of band */
    *lead_par += num_done;
    *lead_blg -= num_done*(args->gap);
    *num_bulges  -= num_done;
	
}

/* chases num_bulges bulges hops times starting with (lead_par,lead_blg) */
void chase_bulges(arguments* args, int* lead_par, int* lead_blg, int* num_bulges)
{
    int i, j, stop, num_done = 0;
    
    /* chase num_bulges bulges */
    for (i = 0; i < *num_bulges; i++)
    {
        stop = 0;
        
        /* chase bulge hops times */
        for (j = 0; !stop && j < args->hops; j++)
        {
            /* if bulge is chased off band, break out of j loop */
            if ( !chase( args, *lead_par+i, *lead_blg+j-i*(args->gap) ) )
            {
                num_done++;
                stop = 1;
            }
        }
    }
    /* increment lead_blg number */
    *lead_blg += args->hops;
    
    /* update parameters if some bulges were chased off end of band */
    *lead_par    += num_done;
    *lead_blg    -= num_done*(args->gap);
    *num_bulges  -= num_done;
	
}

void spin_wait( int thread_id, int n_threads )
{
	if ( thread_id == 0 )
	{
		// make sure not to get ahead of last thread by 2

		while ( set[0] > set[n_threads-1] + 1)
		;
		
		// if first thread starts next set, make sure not to overtake last thread
		while ( set[0] != set[n_threads-1] && last_col[0] >= first_col[n_threads-1] )
		;
	} else {
		// make sure not to get ahead of prev thread

		while ( set[thread_id] > set[thread_id-1] )
		;

		// if prev thread is still working on current set, make sure not to overtake it
		while ( set[thread_id] == set[thread_id-1] && last_col[thread_id] >= first_col[thread_id-1] )
		;
	}	
}

void par_sweep( arguments* args  )
{
	int lead_par, lead_blg, num_bulges, next_lead_par;
	
	/* set min gap between bulges so that bulge (i,j) and (i+1,j-gap) do not overlap */
	set_gap( args );
  
	/* set first parallelogram of first set */
	next_lead_par = (args->thread_id)*(args->mult);
		
	/* loop over sets of num_bulges parallelograms until reach end of band */
	while ( next_lead_par*(args->c) < (args->n)-(args->b)+(args->d)-1 )
	{
	
		/* reset lead_par and num_bulges */
		lead_par = next_lead_par;
		num_bulges = args->mult;
		
		/* compute first col and last col for create_bulges */
		first_col[args->thread_id] = lead_par*(args->c);
		last_col[args->thread_id] = first_col[args->thread_id]+(args->c)-1+(args->b)+((args->mult)-1)*(args->gap)*(args->b);
		set[args->thread_id]++;
				
		/* spin wait to stay behind previous thread */
		spin_wait( args->thread_id, args->n_threads );
						
		/* annihilate set of parallelograms and generate set of packed bulges */
		create_bulges(args,&lead_par,&lead_blg,&num_bulges);
		
		/* compute first col and last col for first chase_bulges */
		first_col[args->thread_id] += ((args->mult)-1)*(args->c)+(args->b)-(args->d);
		last_col[args->thread_id] = MIN( last_col[args->thread_id]+(args->hops)*(args->b), (args->n)+(args->b) );
				
		/* chase set of bulges until reach end of band */
		while (num_bulges > 0)
		{
			/* spin wait to stay behind previous thread */
			spin_wait( args->thread_id, args->n_threads );

			/* chase set of bulges hops times each */
			chase_bulges(args,&lead_par,&lead_blg,&num_bulges);
			
			/* increment first col and last col for next chase_bulges */
			first_col[args->thread_id] += (args->hops)*(args->b);
			last_col[args->thread_id] = last_col[args->thread_id]+(args->hops)*(args->b);			
		}
		
		/* advance to next set of parallelograms */
		next_lead_par += (args->n_threads)*(args->mult);
	}
	
	/* increment set again in case thread does one fewer than others */
	set[args->thread_id]++;
}
