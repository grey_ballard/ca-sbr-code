#ifndef __AUX_H__
#define __AUX_H__

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>

/* Auxiliary routines */
double wall_time();
void print_matrix(double*, int, int, int);
void die(const char*);

#endif
