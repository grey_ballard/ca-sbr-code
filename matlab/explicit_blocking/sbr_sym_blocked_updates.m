function [T U] = sbr_sym_blocked_updates(A,bw)
% usage: T = sbr_sym_blocked(A,bw)
% reduces symmetric matrix A of bandwidth bw to tridiagonal with blocked SBR
% requires A to be symmetric and banded with only lower band stored


    n = size(A,1);
    T = A;
    
    U = eye(n);
	
    % set initial bandwidth and first l to be multiple of 3
    % note: l can be larger as long as 3lb^2 \leq M
	b = bw; l = 3;
    % set cache block width
    width = l*b;
    
    %figure(1), spy_cache_blocks(A,width);
    %figure(2), spy(U);
    %pause;
    
	while b > 1,
        % set c = d = b/2 (if possible)
        % note: choosing to floor d so that next b is divisible by 2
        c = ceil(b/2); d = floor(b/2);
        % set fi to be first parallelogram of each set
        fi = 1;
        % for each block
        for j = 1:ceil(n/(l*b))-2,
            % for each set of paralleograms
            for p = 1:3,
                % set fj to be last bulge of fi
                fj = l*(j+1)-ceil(fi/2);
                % create set of bulges - leading bulge is (fi,fj)
                [T,U] = create_bulges(T,U,fi,fj,b,l,c,d,width);
                % for all remaining blocks (up to last two)
                for k = j+1:ceil(n/(l*b))-2,
                    % pass set of bulges along
					[T,U] = pass_bulges(T,U,fi,fj,b,l,c,d,width);
                    fj = fj + l;
                end
                % clear set of bulges from last two blocks
                [T,U] = clear_bulges(T,U,fi,fj,b,l,c,d,width);
                % increment first parallelogram for next set
                fi = fi + 2*l/3;
            end
        end
        % create parallelograms and clear bulges for last two blocks
        [T,U] = clear_last_bulges(T,U,fi,b,l,c,d,width);
        % reduce current bandwidth by factor of 2
        b = b - d;
        % increase block size by factor of 2
        % note: could increase l by factor of 4 and still fit in cache
        l = l*2;
        % update cache block width
        width = l*b;
	end

    % check for correctness
    norm( U*(T+tril(T,-1)')*U' - (A+tril(A,-1)') )
    norm( sort(eig(T+tril(T,-1)')) - sort(eig(A+tril(A,-1)')) )
    norm( U'*U - eye(n) )
    
end

function [A,U] = create_bulges(A,U,fi,lj,b,l,c,d,width)
% create 2l/3 bulges starting with (fi,0) and chase into next block
% the bulges remaining are, from upper left to lower right,
%    (fi+2*l/3-1,lj-4*l/3+2), ... , (fi+1,lj-2), (fi,lj)

    for i = fi:fi+2*l/3-1,
        for j = 0:lj-1,
            [A,U] = chase(A,U,i,j,b,c,d,width);
        end
        lj = lj-2;
    end

end

function [A,U] = pass_bulges(A,U,fi,fj,b,l,c,d,width)
% chase 2l/3 bulges starting with (fi,fj) into next block

    for i = fi:fi+2*l/3-1,
       for j = fj:fj+l-1,
           [A,U] = chase(A,U,i,j,b,c,d,width);
       end
       fj = fj-2;
    end

end

function [A,U] = clear_bulges(A,U,fi,fj,b,l,c,d,width)
% clear 2l/3 bulges from 2nd to last block

    for i = fi:fi+2*l/3-1,
        for j = fj:fj+2*l,
           [A,U] = chase(A,U,i,j,b,c,d,width);
        end
        fj = fj-2;
    end

end

function [A,U] = clear_last_bulges(A,U,fi,b,l,c,d,width)
% create and clear bulges from last two blocks

    for i = fi:fi+4*l,
       for j = 0:2*l,
          [A,U] = chase(A,U,i,j,b,c,d,width);
       end
    end

end

function [A,U] = chase(A,U,i,j,b,c,d,width)
% chase d x c parallelogram (i,j) where A has bandwidth b

    n = size(A,1);

    if j == 0,
        ltcol = 1 + (i-1)*c;
        rtcol = i*c;
        tprow = ltcol + b - d;
        btrow = min( rtcol + b, n );
    else
    	ltcol = 1 + (i-1)*c + j*b - d;
        rtcol = i*c + j*b - d;
        tprow = ltcol + b;
        btrow = min( rtcol + b + d, n );
    end

    if tprow <= btrow && ltcol <= rtcol,
        
        % band reduction
        [Q R] = qr(A(tprow:btrow,ltcol:rtcol));
        A(tprow:btrow,ltcol:rtcol)     =  R;
        A(tprow:btrow,rtcol+1:tprow-1) =  PRE( A(tprow:btrow,rtcol+1:tprow-1) , Q);
        A(tprow:btrow,tprow:btrow)     =  SYM( A(tprow:btrow,tprow:btrow)     , Q);
        A(btrow+1:end,tprow:btrow)     = POST( A(btrow+1:end,tprow:btrow)     , Q); 
        
        % orthogonal updates
        U(:,tprow:btrow) = POST( U(:,tprow:btrow), Q );
        
        figure(1), spy_cache_blocks(A,width);
        figure(2), spy(U);
        %pause;
    end

end

function A = PRE(A,Q)
% A = Q'*A

    A = Q'*A;
    
end

function A = SYM(A,Q)
% A = Q'*A*Q
% A is stored as lower triangle, B is returned as lower triangle
    
    A = tril( Q' * (A+tril(A,-1)') * Q );

end

function A = POST(A,Q)
% A = A*Q

    A = A*Q;
    
end

function spy_cache_blocks(A,width)

    n = size(A,1);

	spy(A); 
	hold on; 
    for i = 0:n/width,
        plot([i*width i*width], [0 n],'r');
    end
    hold off;

end