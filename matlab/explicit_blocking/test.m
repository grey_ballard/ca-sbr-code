function [A T U] = test(n,b)
% tests explicit blocking algorithms, need b to be power of 2

    % create a banded matrix (lower half)
    A = symbandlower(n,b);

    %T = sbr_sym_blocked(A,b);
    [T U] = sbr_sym_blocked_updates(A,b);
    
end