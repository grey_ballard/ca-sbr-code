/*
 * =====================================================================
 * sbr_mult_c.c  chases bulges, matrix stored in packed format
 * =====================================================================
 */

#include "mex.h"
#include "lapack.h"
#include "blas.h"
#include <stddef.h> /* for int */

#define floor(a,b) (a)/(b)
#define ceil(a,b) ( ((a)%(b) == 0) ? (a)/(b) : ((a)/(b)+1)  )
#define min(a,b) ( (a) < (b) ? (a) : (b) )

#define bs 48

/*
#define CHASEFCN chase3_tight
#define LDBEXTRA 0
*/

/*
#define CHASEFCN chase2
#define LDBEXTRA 0
*/

/*
#define CHASEFCN chase3_extra
#define LDBEXTRA ceil(b,2)
*/

#define CHASEFCN chase3_blocked
#define LDBEXTRA 0


int chase3_tight( double* A, int lda, int n, int b, int c, int d, int par, int blg, double* work )
{
    int i, j, qr_cols, Hh_len, pre_cols, post_rows, cd = c+d, U_len = cd*c, ldar = lda-1, ione = 1;
    char *LL = "L", *RR = "R", *FF = "F", *BB = "B", *CC = "C", *UU = "U", *NN = "N", *TT = "T";
    double dzero = 0.0, done = 1.0, dmone = -1.0, dhalf = 0.5, dmhalf = -0.5;
    double *Ast, *Aqr, *Apre, *Asym, *Apost3, *Apost2, *U, *T, *V, *tau;
    
    /* if parallelogram or bulge is off the band, return zero */
    if (par*c + blg*b >= n-b+d-1)
        return 0;
    
    /* Set pointers to QR/PRE/SYM/POST */ 
    Aqr = A + (b-d) + (par*c)*lda;
    if (blg)
        Aqr += d + (blg*b-d)*lda;
    Apre = Aqr+c*ldar;
    if (blg)
        Asym = Aqr+b*ldar;
    else
        Asym = Aqr+(b-d)*ldar;
    Apost3 = Asym + cd;
    Apost2 = Apost3 + b-c + lda;
    
    /* Set sizes of QR/PRE/POST (SYM is cd x cd) */
    Hh_len = d+1;
    qr_cols = c-1;
    if (blg)
        pre_cols = b-c;
    else
        pre_cols = b-d-c;
    post_rows = b-c+1;
    
    /* Set pointers to workspace (use V for workspace in dlarf/dlarfb) */
    /* total space required is (c+d)*c + c*c + c + (c+d)*b */
    U    = work;
    T    = U + cd*c;
    tau  = T + c*c;
    V    = tau + c;
    
    /* Clear U */
    for (i=0; i<cd*c; i++)
        U[i] = dzero;
    
    /* QR */    
    for (i = 0; i < c; i++)
    {
        /* compute Householder vector */
        dlarfg_( &Hh_len, Aqr+i*lda, Aqr+i*lda+1, &ione, tau+i);
        
        /* copy Householder info to U, make sure other entries of U are zero (don't clear from band yet) */
        U[i+cd*i] = done;
        dcopy_( &d, Aqr+1+i*lda, &ione, U+1+i+i*cd, &ione );
        
        /* apply Hh vector to remaining columns in QR */
        dlarf_( LL, &Hh_len, &qr_cols, U+i+i*cd, &ione, tau+i, Aqr+(i+1)*lda-1, &ldar, V );
        
        qr_cols--;
    }   
        
    /* form T */
    dlarft_( FF, CC, &cd, &c, U, &cd, tau, T, &c );
       
    /* PRE */
    dlarfb_( LL, TT, FF, CC, &cd, &pre_cols, &c, U, &cd, T, &c, Apre, &ldar, V, &pre_cols );
    
    /* POST */
    /* BLAS 3 part */
    dlarfb_( RR, NN, FF, CC, &post_rows, &cd, &c, U, &cd, T, &c, Apost3, &ldar, V, &post_rows );
    /* BLAS 2 part */
    post_rows = 1;
    for (i = 1; i < c; i++)
    {
        dlarf_( RR, &post_rows, &Hh_len, U+i+i*cd, &ione, tau+i, Apost2+(i-1)*ldar, &ldar, V );
        post_rows++;
    }
    
    /* SYM */ 
    /* U = U*T */
    dtrmm_( RR, UU, NN, NN, &cd, &c, &done, T, &c, U, &cd );
    /* V = Asym * U */
    dsymm_( LL, LL, &cd, &c, &done, Asym, &ldar, U, &cd, &dzero, V, &cd );
    /* T = (1/2) * V^T * U */
    dgemm_( TT, NN, &c, &c, &cd, &dhalf, V, &cd, U, &cd, &dzero, T, &c );
    
    /* reset U with Hh vectors and clear from band */
    for (i=0; i<c; i++)
    {
        for (j=0; j<i; j++)
            U[j+i*cd] = dzero;
        U[i+i*cd] = done; 
        dcopy_( &d, Aqr+1+i*lda, &ione, U+1+i+i*cd, &ione );
        for (j=1; j<Hh_len; j++)
            Aqr[j+i*lda] = dzero; 
    }
    
    /* V = V - U * T */
    dgemm_( NN, NN, &cd, &c, &c, &dmone, U, &cd, T, &c, &done, V, &cd );
    /* Asym = Asym - V*U^T - U*V^T */
    dsyr2k_( LL, NN, &cd, &c, &dmone, U, &cd, V, &cd, &done, Asym, &ldar );
        
    /* return successful chase */
    return 1;
    
}

int chase3_extra( double* A, int lda, int n, int b, int c, int d, int par, int blg, double* work )
{
    int i, j, qr_cols, Hh_len, pre_cols, post_rows, cd = c+d, U_len = cd*c, ldar = lda-1, ione = 1, info;
    char *LL = "L", *RR = "R", *FF = "F", *BB = "B", *CC = "C", *UU = "U", *NN = "N", *TT = "T";
    double dzero = 0.0, done = 1.0, dmone = -1.0, dhalf = 0.5, dmhalf = -0.5;
    double *Ast, *Aqr, *Apre, *Asym, *Apost, *U, *T, *V, *tau;
    
    /* if parallelogram or bulge is off the band, return zero */
    if (par*c + blg*b >= n-b+d-1)
        return 0;
    
    /* Set pointers to QR/PRE/SYM/POST */ 
    Aqr = A + (b-d) + (par*c)*lda;
    if (blg)
        Aqr += d + (blg*b-d)*lda;
    Apre = Aqr+c*ldar;
    if (blg)
        Asym = Aqr+b*ldar;
    else
        Asym = Aqr+(b-d)*ldar;
    Apost = Asym + cd;
    
    /* Set sizes of QR/PRE/POST (SYM is cd x cd) */
    Hh_len = d+1;
    qr_cols = c;
    if (blg)
        pre_cols = b-c;
    else
        pre_cols = b-d-c;
    post_rows = b;
    
    /* Set pointers to workspace (use V for workspace in dlarf/dlarfb) */
    /* total space required is (c+d)*c + c*c + c + (c+d)*b */
    U    = work;
    T    = U + cd*c;
    tau  = T + c*c;
    V    = tau + c;
    
    /* Clear U */
    for (i=0; i<cd*c; i++)
        U[i] = dzero;
    
    /* QR */  
    dgeqrf_( &cd, &qr_cols, Aqr, &ldar, tau, V, &U_len, &info );
    
    /* copy Hh vectors to U */
    for (i=0; i<c; i++)
    {
        U[i+i*cd] = done; 
        dcopy_( &d, Aqr+1+i*lda, &ione, U+1+i+i*cd, &ione ); 
    }
        
    /* form T */
    dlarft_( FF, CC, &cd, &c, U, &cd, tau, T, &c );
       
    /* PRE */  
    dlarfb_( LL, TT, FF, CC, &cd, &pre_cols, &c, U, &cd, T, &c, Apre, &ldar, V, &pre_cols );
    
    /* POST */    
    dlarfb_( RR, NN, FF, CC, &post_rows, &cd, &c, U, &cd, T, &c, Apost, &ldar, V, &post_rows );
    
    /* SYM */ 
    /* U = U*T */
    dtrmm_( RR, UU, NN, NN, &cd, &c, &done, T, &c, U, &cd );
    /* V = Asym * U */
    dsymm_( LL, LL, &cd, &c, &done, Asym, &ldar, U, &cd, &dzero, V, &cd );
    /* T = (1/2) * V^T * U */
    dgemm_( TT, NN, &c, &c, &cd, &dhalf, V, &cd, U, &cd, &dzero, T, &c );
    
    /* reset U with Hh vectors and clear from band */
    for (i=0; i<c; i++)
    {
        for (j=0; j<i; j++)
            U[j+i*cd] = dzero;
        U[i+i*cd] = done; 
        dcopy_( &d, Aqr+1+i*lda, &ione, U+1+i+i*cd, &ione );
        for (j=1; j<Hh_len; j++)
            Aqr[j+i*lda] = dzero; 
    }
    
    /* V = V - U * T */
    dgemm_( NN, NN, &cd, &c, &c, &dmone, U, &cd, T, &c, &done, V, &cd );
    /* Asym = Asym - V*U^T - U*V^T */
    dsyr2k_( LL, NN, &cd, &c, &dmone, U, &cd, V, &cd, &done, Asym, &ldar );
         
    return 1;
    
}

/* chase one bulge, if bulge off end of band, return false */
int chase2( double* A, int lda, int n, int b, int c, int d, int par, int blg, double* work )
{
    int i, j, Hh_len = d+1, Hh_num, qr_cols, pre_cols, post_rows, ldar = lda-1, ione = 1;
    char *L = "L", *R = "R";
    double alpha, dzero = 0.0, done = 1.0, dmone = -1.0, dmhalf = -0.5;
    
    /* if parallelogram or bulge is off the band, return zero */
    if (par*c + blg*b >= n-b+d-1)
        return 0;
    
    double *Ast, *Aqr, *Apre, *Asym, *Asym_pre, *Asym_post, *Apost, *tau, *diag;
    
    /* Divy up workspace to tau (for Hh vectors), diag (for storing accumulators), and workspace for LAPACK functions */
    tau = work;
    diag = work+c;
    work = work+2*c;
    
    /* Set Ast to first entry of parallelogram or bulge */
    Ast = A + (b-d) + (par*c)*lda;
    if (blg)
        Ast += d + (blg*b-d)*lda;
    
    /* QR */
    Aqr = Ast;
    qr_cols = c-1;
    for (i = 0; i < c; i++)
    {
        /* compute Householder vector */
        dlarfg_( &Hh_len, Aqr, Aqr+1, &ione, tau+i);

        /* store accumulated value and overwrite with explicit one */
        diag[i] = *Aqr;
        *Aqr = done;  
        
        /* apply Hh vector to remaining columns in QR */
        dlarf_( L, &Hh_len, &qr_cols, Aqr, &ione, tau+i, Aqr+ldar, &ldar, work );
        
        /* advance Aqr down diagonal */
        Aqr += lda;
        
        qr_cols--;
    }
    
    /* PRE */
    Aqr = Ast;
    Apre = Ast+c*ldar;
    if (blg)
        pre_cols = b-c;
    else
        pre_cols = b-d-c;
    for (i = 0; i < c; i++)
    {
        dlarf_( L, &Hh_len, &pre_cols, Aqr, &ione, tau+i, Apre, &ldar, work );
        
        /* advance Apre down column and Aqr down diagonal */
        Apre++;
        Aqr += lda;
    }
    
    /* SYM */
    Aqr = Ast;
    if (blg)
        Asym = Ast+b*ldar;
    else
        Asym = Ast+(b-d)*ldar;
    Asym_pre = Asym;
    Asym_post = Asym+d+1;
    pre_cols = 0; post_rows = c-1;
    for (i = 0; i < c; i++)
    {     
        /* PRE inside SYM */
        dlarf_( L, &Hh_len, &pre_cols, Aqr, &ione, tau+i, Asym_pre, &ldar, work );
        
        /* SYM inside SYM */
        dsymv_( L, &Hh_len, tau+i, Asym, &ldar, Aqr, &ione, &dzero, work, &ione );
        alpha = (-0.5)*tau[i]*ddot_( &Hh_len, work, &ione, Aqr, &ione );
        daxpy_( &Hh_len, &alpha, Aqr, &ione, work, &ione );
        dsyr2_( L, &Hh_len, &dmone, Aqr, &ione, work, &ione, Asym, &ldar );
        
        /* POST inside SYM */
        dlarf_( R, &post_rows, &Hh_len, Aqr, &ione, tau+i, Asym_post, &ldar, work );
        
        /* advance Asym, Asym_post, and Aqr down diagonals and Asym_pre down column */
        Asym += lda;
        Asym_pre += 1;
        Asym_post += lda;
        Aqr += lda;
        
        pre_cols++;
        post_rows--;
    }
    
    /* POST */
    Aqr = Ast;
    if (blg)
        Apost = Ast+(c+d)+b*ldar;
    else
        Apost = Ast+(c+d)+(b-d)*ldar;
    post_rows = b-c+1;
    for (i = 0; i < c; i++)
    {
        dlarf_( R, &post_rows, &Hh_len, Aqr, &ione, tau+i, Apost, &ldar, work );
        
        /* advance Apost along row and Aqr down diagonal */
        Apost += ldar;
        Aqr += lda;
        
        post_rows++;
    }
    
    /* clean up (restore accumulators and delete Hh entries) */
    Aqr = Ast;
    for (i = 0; i < c; i++)
    {
        Aqr[0] = diag[i];
        for( j = 1; j <= d; j++)
            Aqr[j] = 0;
        Aqr += lda;
    }
    
    return 1;
    
}

/* block square bulge into tall skinny slabs and use BLAS3 functions */
int chase3_blocked( double* A, int lda, int n, int b, int c, int d, int par, int blg, double* work )
{
    int i, j, k, qr_cols, Hh_len = d+1, pre_cols, post_rows, cc, ccd, num_slabs, U_len, cd = c+d, ldar = lda-1, ione = 1;
    char *LL = "L", *RR = "R", *FF = "F", *BB = "B", *CC = "C", *UU = "U", *NN = "N", *TT = "T";
    double dzero = 0.0, done = 1.0, dmone = -1.0, dhalf = 0.5, dmhalf = -0.5;
    double *Ast, *Aqr, *Apre, *Asym, *Apost3, *Apost2, *U, *T, *V, *tau;
    
    /* if parallelogram or bulge is off the band, return zero */
    if (par*c + blg*b >= n-b+d-1)
        return 0;
    
    /* Set cc to be number of cols of first slab */
    cc = min(c,bs);
    ccd = cc+d;
    U_len = ccd*cc;
    
    /* Set pointers to workspace (use V for workspace in dlarf/dlarfb) */
    /* total space required is (bs+d)*bs + bs*bs + bs + (bs+d)*b */
    U    = work;
    T    = U + ccd*cc;
    tau  = T + cc*cc;
    V    = tau + cc;
      
    /* Set initial pointers to QR/PRE/SYM/POST */ 
    Aqr = A + (b-d) + (par*c)*lda;
    if (blg)
        Aqr += d + (blg*b-d)*lda;
    Apre = Aqr+cc*ldar;
    if (blg)
        Asym = Aqr+b*ldar;
    else
        Asym = Aqr+(b-d)*ldar;
    Apost3 = Asym + ccd;
    Apost2 = Apost3 + b-cc + lda;
    
    /* loop over slabs */
    num_slabs = ceil(c,bs);
    for (k = 0; k < num_slabs; k++)
    {
        
        /* Set sizes of QR/PRE/POST (SYM is ccd x ccd) */
        qr_cols = cc-1;
        if (blg)
            pre_cols = b-cc; 
        else
            pre_cols = b-d-cc;
        post_rows = b-cc+1;
        
        /* Clear U */
        for (i=0; i<ccd*cc; i++)
            U[i] = dzero;

        /* QR */    
        for (i = 0; i < cc; i++)
        {
            /* compute Householder vector */
            dlarfg_( &Hh_len, Aqr+i*lda, Aqr+i*lda+1, &ione, tau+i);

            /* copy Householder info to U, make sure other entries of U are zero (don't clear from band yet) */
            U[i+ccd*i] = done;
            dcopy_( &d, Aqr+1+i*lda, &ione, U+1+i+i*ccd, &ione );

            /* apply Hh vector to remaining columns in QR */
            dlarf_( LL, &Hh_len, &qr_cols, U+i+i*ccd, &ione, tau+i, Aqr+(i+1)*lda-1, &ldar, V );

            qr_cols--;
        }   

        /* form T */
        dlarft_( FF, CC, &ccd, &cc, U, &ccd, tau, T, &cc );

        /* PRE */
        dlarfb_( LL, TT, FF, CC, &ccd, &pre_cols, &cc, U, &ccd, T, &cc, Apre, &ldar, V, &pre_cols );

        /* POST */
        /* BLAS 3 part */
        dlarfb_( RR, NN, FF, CC, &post_rows, &ccd, &cc, U, &ccd, T, &cc, Apost3, &ldar, V, &post_rows );
        /* BLAS 2 part */
        post_rows = 1;
        for (i = 1; i < cc; i++)
        {
            dlarf_( RR, &post_rows, &Hh_len, U+i+i*ccd, &ione, tau+i, Apost2+(i-1)*ldar, &ldar, V );
            post_rows++;
        }

        /* SYM */ 
        /* U = U*T */
        dtrmm_( RR, UU, NN, NN, &ccd, &cc, &done, T, &cc, U, &ccd );
        /* V = Asym * U */
        dsymm_( LL, LL, &ccd, &cc, &done, Asym, &ldar, U, &ccd, &dzero, V, &ccd );
        /* T = (1/2) * V^T * U */
        dgemm_( TT, NN, &cc, &cc, &ccd, &dhalf, V, &ccd, U, &ccd, &dzero, T, &cc );

        /* reset U with Hh vectors and clear from band */
        for (i=0; i<cc; i++)
        {
            for (j=0; j<i; j++)
                U[j+i*ccd] = dzero;
            U[i+i*ccd] = done; 
            dcopy_( &d, Aqr+1+i*lda, &ione, U+1+i+i*ccd, &ione );
            for (j=1; j<Hh_len; j++)
                Aqr[j+i*lda] = dzero; 
        }

        /* V = V - U * T */
        dgemm_( NN, NN, &ccd, &cc, &cc, &dmone, U, &ccd, T, &cc, &done, V, &ccd );
        /* Asym = Asym - V*U^T - U*V^T */
        dsyr2k_( LL, NN, &ccd, &cc, &dmone, U, &ccd, V, &ccd, &done, Asym, &ldar );
        
        /* set cc to be number of columns annihilated in next pass */
        cc = min( c-(k+1)*bs, bs);
        ccd = cc+d;
        
        /* increment pointers for next slab */
        Aqr    += bs*lda;
        Apre   += bs*lda - (bs-cc)*ldar;
        Asym   += bs*lda;
        Apost3 += bs*lda - (bs-cc);
        Apost2 += bs*lda;
    }
        
    /* return successful chase */
    return 1;

}


/* creates num_bulges bulges starting with bulge (lead_par,0)  */
void create_bulges(double* A, int lda, int n, int b, int c, int d, int* lead_par, int* lead_blg, int* num_bulges, int gap, double* work)
{
    int i, k, done, num_done = 0; 
    *lead_blg = 1-gap;

    /* create num_bulges bulges */
    for (i = 0; i < *num_bulges; i++)
    {
        /* make room for ith bulge */
        /* (start k at num_done to avoid chasing bulges already off end of band) */
        for (k = num_done; k < i; k++)
        {
            done = 0;
            
            /* chase previously created bulge once */
            //mexPrintf("par=%d, blg=%d\n",*lead_par+k,*lead_blg-gap*k);
            if (!CHASEFCN(A,lda,n,b,c,d,*lead_par+k,*lead_blg-gap*k,work))
            {
                num_done++;
                done = 1;
            }
            /* if gap is 2 then must chase it again */
            /* (also check to see if the first chase pushed it off end of band) */
            if (gap == 2 && !done)
            {
                //mexPrintf("par=%d, blg=%d\n",*lead_par+k,*lead_blg-gap*k+1);
                if (!CHASEFCN(A,lda,n,b,c,d,*lead_par+k,*lead_blg-gap*k+1,work))
                    num_done++;
            }
        }
        /* create ith bulge */
        //mexPrintf("par=%d, blg=%d\n",*lead_par+i,0);
        if (!CHASEFCN(A,lda,n,b,c,d,*lead_par+i,0,work))
            num_done++;
        /* increment lead_blg number */
        *lead_blg += gap;
    }
    //mexPrintf("num_done=%d\n",num_done);

    /* update parameters if some bulges were chased off end of band */
    *lead_par += num_done;
    *lead_blg -= num_done*gap;
    *num_bulges  -= num_done;

}

/* chases num_bulges bulges hops times starting with (lead_par,lead_blg) */
void chase_bulges(double* A, int lda, int n, int b, int c, int d, int* lead_par, int* lead_blg, int* num_bulges, int gap, int hops, double* work)
{
    int i, j, stop, num_done = 0;
    
    /* chase num_bulges bulges */
    for (i = 0; i < *num_bulges; i++)
    {
        stop = 0;
        
        /* chase bulge hops times */
        for (j = 0; !stop && j < hops; j++)
        {
            /* if bulge is chased off band, break out of j loop */
            //mexPrintf("par=%d, blg=%d\n",*lead_par+i,*lead_blg+j-i*gap);
            if (!CHASEFCN(A,lda,n,b,c,d,*lead_par+i,*lead_blg+j-i*gap,work))
            {
                num_done++;
                stop = 1;
            }
        }
    }
    /* increment lead_blg number */
    *lead_blg += hops;
    
    /* update parameters if some bulges were chased off end of band */
    *lead_par    += num_done;
    *lead_blg    -= num_done*gap;
    *num_bulges  -= num_done;
            
}


void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
    int i,j,k,p;
    int n,l,b,c,d,ldb,fi,fj;
    
	double *A;
    double *T;
    
    double *B, *work;
    
    if (nrhs != 2) 
        mexErrMsgTxt("2 inputs required.");
    if (nlhs != 1) 
        mexErrMsgTxt("1 output required.");
    
    n = mxGetM(prhs[0]);
    
    if (n != mxGetN(prhs[0]))   
        mexErrMsgTxt("Matrix must be square.");
    
    /* Get the matrix */
    A = mxGetPr(prhs[0]);
    
    /* Get the bandwidth */
    b = *mxGetPr(prhs[1]);
     
	/* Initialize output. */
    plhs[0] = mxCreateDoubleMatrix(n,n,mxREAL);
	T = mxGetPr(plhs[0]);
    
    /* Allocate internal packed array (with extra space) 
       - need d_1 = floor(bw,2) extra diagonals for bulges 
       - need bw more columns at the end of the band to 
         let chase() work on zeroes for corner cases */
    ldb = 1+b+b/2 + LDBEXTRA;
    B = mxCalloc( (n+b) * ldb, sizeof(double) );  
    
    /* Copy input matrix to packed storage */
    for (i = 0; i < n; i++)
    {
        int length = b+1;
        if (i >= n-b ) length = n - i;
        for (j = 0; j < length; j++)
        {
            B[j+i*ldb] = A[j+i+i*n];
        }
    }
    
    /* Allocate workspace */
    work = mxCalloc( 3 * b * b, sizeof(double) );
    
    int lead_par, lead_blg, num_bulges, next_lead_par, desired_num_bulges, hops, gap;
    
    /* loop over multiple sweeps until tridiagonal */
    while (b > 1 && 0)
    {
        /* set c and d to be b/2 (if possible) */
        c = ceil(b,2); d = floor(b,2);
        
        /* for general choices of c and d, set min gap between bulges 
           so that bulge (i,j) and (i+1,j-gap) do not overlap */
        if (2*c+d <= b)
            gap = 1;
        else
            gap = 2;   
        
        /* set number of bulges to chase and how far to chase them at each step */
        /* note: should probably change at every sweep, but stays constant within sweep */
        desired_num_bulges = 3;
        hops = 2;
        
        /* start with parallelogram 0 */
        next_lead_par = 0;
        
        /* loop over sets of num_bulges parallelograms until reach end of band */
        while (next_lead_par*c < n-b+d-1)
        {
            /* reset lead_par and num_bulges */
            lead_par = next_lead_par;
            num_bulges = desired_num_bulges;
            
            /* annihilate set of parallelograms and generate set of packed bulges */
            create_bulges(B,ldb,n,b,c,d,&lead_par,&lead_blg,&num_bulges,gap,work);
            
            /* chase set of bulges until reach end of band */
            while (num_bulges > 0)
                chase_bulges(B,ldb,n,b,c,d,&lead_par,&lead_blg,&num_bulges,gap,hops,work);
            
            /* advance to next set of parallelograms */
            next_lead_par += desired_num_bulges;
        }
        
        /* compute size of remaining band */
        b = b-d;
    }
    
    c=ceil(b,2); d=floor(b,2);
    chase3_blocked(B, ldb, n, b, c, d, 0, 0, work);
    //chase3_blocked(B, ldb, n, b, c, d, 0, 1, work);
    
    /* Copy packed storage to output matrix */
    for(i=0; i<n*n; i++) T[i] = 0;
    for (i = 0; i < n; i++)
    {
        int length = ldb;
        if (i >= n-ldb ) length = n - i;
        for (j = 0; j < length; j++)
        {
            T[j+i+i*n] = B[j+i*ldb];
        }
    }
    
    mxFree( work );
    
	mxFree( B );
    
}

