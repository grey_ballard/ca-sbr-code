function A = symbandlower(n,b)
% usage: A = symbandlower(n,b)
% creates n x n symmetric banded matrix with bandwidth b
% only "stores" lower half of matrix

    % return randn to default initial state (for repeatability)
    randn('state',0);

    % set main diagonal
    v = randn(1,n);
    A = diag(v);

    % set b off diagonals
    for i=1:b,
        v = randn(1,n-i);
        A = A + diag(v,-i);
    end

end