function T = sbr_mult(A,b)
% usage: T = sbr_mult(A,b)
% reduces symmetric matrix A of bandwidth b to tridiagonal with blocked SBR
% requires A to be symmetric and banded with only lower band stored

    n = size(A,1);
    bw = b;
    T = A;
	
	while bw > 1,
        c = ceil(bw/2); d = floor(bw/2);
        if 2*c+d <= bw,
            gap = 1;
        else
            gap = 2;
        end        
        orig_numblgs = 4;
        hops = 3;
        
        orig_leadi = 0;
        while orig_leadi*c < n-bw+d-1,
            leadi = orig_leadi;
            numblgs = orig_numblgs;
            [T leadi leadj numblgs] = create_bulges(T,bw,c,d,leadi,numblgs,gap);
            while numblgs > 0,
                [T leadi leadj numblgs] = chase_bulges(T,bw,c,d,leadi,leadj,numblgs,hops,gap);
            end
            orig_leadi = orig_leadi + orig_numblgs;
        end
        bw = bw-d;
	end

    % check for correctness
    norm(sort(eig(T+tril(T,-1)'))-sort(eig(A+tril(A,-1)')));
    
end

function [A leadi leadj numblgs] = create_bulges(A,b,c,d,leadi,numblgs,gap)
% creates numblgs bulges starting with (leadi,0)
% for c code, convert par arg in chase calls to 0-indexing (remove +1's)

    numdone = 0;
    leadj = -gap;
    for i=0:numblgs-1, 
       for k=numdone:i-1,
           [A done]=chase(A,b,c,d,leadi+k+1,leadj-gap*k+1);
           if done == 1,
                numdone = numdone+1;
           end
           if gap == 2,
               if done == 0,
                   [A done]=chase(A,b,c,d,leadi+k+1,leadj-2*k+2);
                   if done == 1,
                       numdone = numdone+1;
                   end
               end
           end
       end
       [A done]=chase(A,b,c,d,leadi+i+1,0);
       if done == 1,
           numdone = numdone+1;
       end
       leadj = leadj+gap;
    end
    leadj = leadj+1;
    
    % update parameters if some bulges were chased off end of band
    leadi = leadi+numdone;
    leadj = leadj-numdone*gap;
    numblgs = numblgs-numdone;

end

function [A leadi leadj numblgs] = chase_bulges(A,b,c,d,leadi,leadj,numblgs,hops,gap)
% chases numblgs bulges hops times starting with (leadi,leadj)
% for c code, convert par arg in chase calls to 0-indexing (remove +1's)

    numdone = 0; stop = 0;
    for i=0:numblgs-1,
        for j=0:hops-1,
            if stop == 0,
                [A done]=chase(A,b,c,d,leadi+i+1,leadj+j-gap*i);
                if done == 1,
                    numdone = numdone+1;
                    stop = 1; % to break out of j loop
                end
            end
        end
        stop = 0;
    end
    leadj = leadj + hops;
    
    % update parameters if some bulges were chased off end of band
    leadi = leadi+numdone;
    leadj = leadj-numdone*gap;
    numblgs = numblgs-numdone;
    
end

function [A done] = chase(A,b,c,d,i,j)
% chase d x c parallelogram (i,j) where A has bandwidth b

    n = size(A,1);
    
    if( (i-1)*c + j*b >= n-b+d-1 )
        done = 1;
        return;
    else
        done = 0;
    end

    if j == 0,
        ltcol = 1 + (i-1)*c;
        rtcol = i*c;
        tprow = ltcol + b - d;
        btrow = min( rtcol + b, n );
    else
    	ltcol = 1 + (i-1)*c + j*b - d;
        rtcol = i*c + j*b - d;
        tprow = ltcol + b;
        btrow = min( rtcol + b + d, n );
    end

    if tprow <= btrow && ltcol <= rtcol,
        [Q R] = qr(A(tprow:btrow,ltcol:rtcol));
        A(tprow:btrow,ltcol:rtcol)     =  R;
        A(tprow:btrow,rtcol+1:tprow-1) =  PRE( A(tprow:btrow,rtcol+1:tprow-1) , Q);
        A(tprow:btrow,tprow:btrow)     =  SYM( A(tprow:btrow,tprow:btrow)     , Q);
        A(btrow+1:end,tprow:btrow)     = POST( A(btrow+1:end,tprow:btrow)     , Q);
        %spy(A); pause; 
    end

end

function A = PRE(A,Q)
% A = Q'*A

    A = Q'*A;
    
end

function A = SYM(A,Q)
% A = Q'*A*Q
% A is stored as lower triangle, B is returned as lower triangle
    
    A = tril( Q' * (A+tril(A,-1)') * Q );

end

function A = POST(A,Q)
% A = A*Q

    A = A*Q;
    
end

