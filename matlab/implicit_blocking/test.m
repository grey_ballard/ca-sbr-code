
results=zeros(1,3);

i=1;
for n=30:50,
    for b = 7:17,
        A = symbandlower(n,b);
        T = sbr_mult(A,b);
        results(i,:) = [n b max( sort(eig(T+tril(T,-1)'))-sort(eig(A+tril(A,-1)')) )];
        i = i+1;
    end
end

maxerror = max(results(:,3))