mex sbr_mult_c.c -lmwlapack -lmwblas

results=zeros(1,3);

i=1;
for n=300:10:350,
    for b = 6:29,
        A = symbandlower(n,b);
        T = sbr_mult_c(A,b);
        results(i,:) = [n b max( sort(eig(T+tril(T,-1)'))-sort(eig(A+tril(A,-1)')) )];
        i = i+1;
    end
end

maxerror = max(results(:,3))
results;

delete('sbr_mult_c.mexmaci')