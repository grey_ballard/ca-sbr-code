#ifndef __CASBR_H__
#define __CASBR_H__

/* define struct for passing arguments through functions */
typedef struct {
	double* A;
	int lda;
	int n;
	int b;
	int c;
	int d;
	int mult;
	int hops;
	int slab;
	int chasefcn;
	int gap;
	double* work;
	int n_threads;
	int thread_id;
	double seconds;
	int thresholds[2];
	int threshold_tol;
	double *band2;
} arguments;

/* global synchronization variables */
volatile int *first_col, *last_col, *set;

/* kernel function definitions */
void set_gap(arguments*);
int chase(arguments*,int,int);
int chase2(double*,int,int,int,int,int,int,int,double*);
int chase3_blocked(double*,int,int,int,int,int,int,int,double*,int);
void create_bulges(arguments*,int*,int*,int*);
void chase_bulges(arguments*,int*,int*,int*);
double max_eigval_diff(int,double*,int,int,double*,int,int,double*);
double max_absval_diff(int,int,double*,int,double*,int);
void par_sweep(arguments*);
void spin_wait(int,int);

/* macros */
#define MIN(a,b) ( (a)<(b) ? (a) : (b) )
#define MAX(a,b) ( (a)>(b) ? (a) : (b) )
#define FLOOR(a,b) ( (a)/(b) )
#define CEIL(a,b) ( ((a)%(b) == 0) ? (a)/(b) : ((a)/(b)+1)  )
#define QUOTEME_(x) #x       // trick to print compile-time constants as strings
#define QUOTEME(x) QUOTEME_(x)

/* BLAS routines needed:
 - DSYMV
 - DAXPY
 - DDOT
 - DSYR2
 - DGEMM
 - DTRMM 
 - DSYMM
 - DSYR2K */

/* LAPACK routines needed:
 - DGEQRF
 - DCOPY
 - DLARFG
 - DLARF
 - DLARFB
 - DLARFT */

/* Default: all FORTRAN semantics */
#ifdef DEFAULT
 /* BLAS routines: */
 #define DSYMV dsymv_
   extern void DSYMV(char*,int*,double*,double*,int*,double*,int*,double*,double*,int*);
 #define DAXPY daxpy_
   extern void DAXPY(int*,double*,double*,int*,double*,int*);
 #define DDOT  ddot_
   extern double DDOT(int*,double*,int*,double*,int*);
 #define DSYR2 dsyr2_
   extern void DSYR2(char*,int*,double*,double*,int*,double*,int*,double*,int*);
 #define DGEMM dgemm_
   extern void DGEMM(char*,char*,int*,int*,int*,double*,double*,int*,double*,int*,double*,double*,int*);
 #define DTRMM dtrmm_
   extern void DTRMM(char*,char*,char*,char*,int*,int*,double*,double*,int*,double*,int*);
 #define DSYMM dsymm_
   extern void DSYMM(char*,char*,int*,int*,double*,double*,int*,double*,int*,double*,double*,int*);
 #define DSYR2K dsyr2k_
   extern void DSYR2K(char*,char*,int*,int*,double*,double*,int*,double*,int*,double*,double*,int*);

 /* LAPACK routines: */
 #define DGEQRF dgeqrf_
   extern void DGEQRF(int*,int*,double*,int*,double*,double*,int*,int*);
 #define DSBEV dsbev_
   extern void DSBEV( char*,char*,int*,int*,double*,int*,double*,double*,int*,double*,int* ); 
 #define DSTEV dstev_
   extern void  DSTEV( char*,int*,double*,double*,double*,int*,double*,int* );
 #define DSBTRD dsbtrd_
   extern void DSBTRD( char*,char*,int*,int*,double*,int*,double*,double*,double*,int*,double*,int* );
 #define DCOPY  dcopy_
   extern void DCOPY(int*,double*,int*,double*,int*);
 #define DLARFG dlarfg_ 
   extern void DLARFG(int*,double*,double*,int*,double*);
 #define DLARFB dlarfb_
   extern void DLARFB(char*,char*,char*,char*,int*,int*,int*,double*,int*,double*,int*,double*,int*,double*,int*);
 #define DLARFT dlarft_
   extern void DLARFT(char*,char*,int*,int*,double*,int*,double*,double*,int*);
 /* NOTE: DO THIS ONE LAST, since "DLARF" is a substring of "DLARF*" */
 #define DLARF  dlarf_
   extern void DLARF(char*,int*,int*,double*,int*,double*,double*,int*,double*);
#endif

#ifdef MKL
 #include <mkl.h>
 #include <mkl_service.h>
 
 #define DSYMV dsymv
 #define DAXPY daxpy
 #define DDOT  ddot
 #define DSYR2 dsyr2
 #define DGEMM dgemm
 #define DTRMM dtrmm
 #define DSYMM dsymm
 #define DSYR2K dsyr2k
 #define DGEQRF dgeqrf
 #define DSBEV dsbev
 #define DSTEV dstev
 #define DSBTRD dsbtrd
 #define DCOPY  dcopy
 #define DLARFG dlarfg 
 #define DLARFB dlarfb
 #define DLARFT dlarft
 #define DLARF  dlarf
 
 // For comparison with PLASMA:
 #define DSYEVR dsyevr
 
// This one never worked:
 /* #define DSBRDB mkl_lapack_dsbrdb
 extern void DSBRDB (char*,char*,int*,int*,double*,int*,double*,double*,double*,int*,double*,int*,int*); */
#endif

#endif
