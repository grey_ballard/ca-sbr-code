#include "aux.h"

double wall_time()
{
  struct timeval t;
  gettimeofday(&t,NULL);
  return 1.*t.tv_sec+1.e-6*t.tv_usec;
}

void print_matrix(double *A, int m, int n, int lda)
{
  int i, j;
  for (i = 0; i < m; ++i)
  {
    for (j = 0; j < n-1; ++j)
      printf("%.3e,", A[i+j*lda]);
    printf("%.3e\n", A[i+j*lda]);
    fflush(stdout);
  }
}

void die(const char *message)
{
  perror(message);
  exit(EXIT_FAILURE);
}
